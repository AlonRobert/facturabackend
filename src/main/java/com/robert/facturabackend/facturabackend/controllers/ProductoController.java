package com.robert.facturabackend.facturabackend.controllers;

import com.robert.facturabackend.facturabackend.entity.Cliente;
import com.robert.facturabackend.facturabackend.entity.Producto;
import com.robert.facturabackend.facturabackend.service.IProductoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/producto")
@SessionAttributes("producto")
public class ProductoController {

    @Autowired
    private IProductoService productoService;

    protected final Log logger = LogFactory.getLog(this.getClass());

    @GetMapping(value = "/listar")
    public @ResponseBody
    List<Producto> listarRest(){
        return productoService.findAll();
    }
}
