package com.robert.facturabackend.facturabackend.serviceImpl;

import com.robert.facturabackend.facturabackend.entity.Cliente;
import com.robert.facturabackend.facturabackend.entity.Producto;
import com.robert.facturabackend.facturabackend.repository.IProductoDao;
import com.robert.facturabackend.facturabackend.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductoServiceImpl implements IProductoService{

    @Autowired
    private IProductoDao productoDao;

    @Override
    public List<Producto> findAll() {
        return (List<Producto>) productoDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Producto> findByNombre(String term) {
        return productoDao.findByNombreLikeIgnoreCase("%"+term+"%");

    }

    @Override
    @Transactional(readOnly=true)
    public Producto findProductoById(Long id) {
        return productoDao.findById(id).orElse(null);

    }

    @Override
    public void save(Producto producto) {
        productoDao.save(producto);
    }

    @Override
    public void delete(Long id) {
        productoDao.deleteById(id);
    }
}
