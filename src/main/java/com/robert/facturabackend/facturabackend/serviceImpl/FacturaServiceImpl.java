package com.robert.facturabackend.facturabackend.serviceImpl;

import com.robert.facturabackend.facturabackend.entity.Factura;
import com.robert.facturabackend.facturabackend.repository.IFacturaDao;
import com.robert.facturabackend.facturabackend.service.IFacturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FacturaServiceImpl implements IFacturaService {

    @Autowired
    private IFacturaDao facturaDao;

    @Override
    @Transactional
    public void saveFactura(Factura factura) {
        facturaDao.save(factura);
    }


    @Override
    @Transactional(readOnly=true)
    public Factura findFacturaById(Long id) {
        return facturaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void deleteFactura(Long id) {
        facturaDao.deleteById(id); // facturaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Factura fetchFacturaByIdWithClienteWhithItemFacturaWithProducto(Long id) {
        return facturaDao.fetchByIdWithClienteWhithItemFacturaWithProducto(id);
    }
}
