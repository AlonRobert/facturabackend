package com.robert.facturabackend.facturabackend.serviceImpl;

import com.robert.facturabackend.facturabackend.entity.Cliente;
import com.robert.facturabackend.facturabackend.entity.Factura;
import com.robert.facturabackend.facturabackend.entity.Producto;
import com.robert.facturabackend.facturabackend.repository.IClienteDao;
import com.robert.facturabackend.facturabackend.repository.IFacturaDao;
import com.robert.facturabackend.facturabackend.repository.IProductoDao;
import com.robert.facturabackend.facturabackend.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteServiceImpl implements IClienteService{

  @Autowired
  private IClienteDao clienteDao;

  @Override
  @Transactional(readOnly = true)
  public List<Cliente> findAll() {
    // TODO Auto-generated method stub
    return (List<Cliente>) clienteDao.findAll();
  }

  @Override
  @Transactional
  public void save(Cliente cliente) {
    clienteDao.save(cliente);

  }

  @Override
  @Transactional(readOnly = true)
  public Cliente findOne(Long id) {
    return clienteDao.findById(id).orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public Cliente fetchByIdWithFacturas(Long id) {
    return clienteDao.fecthByIdWithFacturas(id);
  }

  @Override
  @Transactional
  public void delete(Long id) {
    clienteDao.deleteById(id);

  }

  @Override
  @Transactional(readOnly = true)
  public Page<Cliente> findAll(Pageable pageable) {
    return clienteDao.findAll(pageable);
  }

}
