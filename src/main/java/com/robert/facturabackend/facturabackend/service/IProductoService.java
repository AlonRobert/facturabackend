package com.robert.facturabackend.facturabackend.service;

import com.robert.facturabackend.facturabackend.entity.Cliente;
import com.robert.facturabackend.facturabackend.entity.Producto;

import java.util.List;

public interface IProductoService {

    public List<Producto> findAll();

    public List<Producto> findByNombre(String term);

    public Producto findProductoById(Long id);

    public void save(Producto producto);

    public void delete(Long id);
}
