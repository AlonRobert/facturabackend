package com.robert.facturabackend.facturabackend.service;

import com.robert.facturabackend.facturabackend.entity.Factura;

public interface IFacturaService {

    public void saveFactura(Factura factura);

    public Factura findFacturaById(Long id);

    public void deleteFactura(Long id);

    public Factura fetchFacturaByIdWithClienteWhithItemFacturaWithProducto(Long id);
}
