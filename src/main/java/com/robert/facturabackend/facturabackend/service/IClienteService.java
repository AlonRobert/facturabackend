package com.robert.facturabackend.facturabackend.service;

import com.robert.facturabackend.facturabackend.entity.Cliente;
import com.robert.facturabackend.facturabackend.entity.Factura;
import com.robert.facturabackend.facturabackend.entity.Producto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IClienteService {

  public List<Cliente> findAll();

  public Page<Cliente> findAll(Pageable pageable);

  public void save(Cliente cliente);

  public Cliente findOne(Long id);

  public Cliente fetchByIdWithFacturas(Long id);

  public void delete(Long id);
}
